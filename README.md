# 💤 LazyVim

# Installation de NeoVim

Vous pouvez installer NeoVim en utilisant la commande suivante :

`sudo apt install neovim`

Vérifiez que l’installation a réussi en lançant :

`nvim --version`

Clonage de la configuration Git

Une fois NeoVim installé, vous pouvez cloner votre configuration à partir de Git.

ssh: `git clone git@gitlab.com:Vodianoi/nvim.git ~/.config/nvim`

Cela clonera votre configuration dans le répertoire de configuration de NeoVim.
Utilisation de NeoVim

Vous pouvez maintenant lancer NeoVim avec votre configuration en utilisant la commande :

`nvim`

[Keybind](https://gitlab.com/m0sz/nvim/-/blob/main/doc/keybind.md)
