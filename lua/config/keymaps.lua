-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here
local keymap = vim.keymap

-- Select all
keymap.set("n", "<C-a>", "gg<S-v>G")
-- CtrlZ
keymap.set("i", "<C-z>", "<esc><cmd>:u<CR>")

keymap.set("n", "²", ":")
keymap.set("n", "œ", ":")
keymap.set("n", "C-k", "<cmd>m .-2<cr>==", { desc = "Move up" })
keymap.set("i", "C-k", "<esc>cmd .-2<cr>==gi", { desc = "Move up" })
keymap.set("v", "C-k", ":m '<-2<cr>gv=gv'", { desc = "Move up" })

keymap.set('n', '<S-C-Down>', 'ddp', { noremap = true, silent = true })
keymap.set('n', '<S-C-Up>', 'ddkP', { noremap = true, silent = true })

