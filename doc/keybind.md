Conf nvim perso : [Configuration](https://gitlab.com/m0sz/nvim)

(`leader` = espace)

| Thème | Commande | Description |
| ---- | ---- | ---- |
| Navigation | `left` `down` `up` `right` | Se déplacer à gauche, en bas, en haut, à droite respectivement |
| Navigation | `w` `b` | Aller au mot suivant/précédent |
| Navigation | `gg` `G` | Aller au début/fin du fichier |
| Édition | `i` `a` | Passer en mode insertion avant/après le curseur |
| Édition | `o` / `O` | Insérer une nouvelle ligne après/avant la ligne actuelle |
| Édition | `u` / `Ctrl + r` | Annuler/rétablir |
| Recherche | `/` `n` `N` | Rechercher, aller à l'occurrence suivante/précédente |
| Enregistrement et Quitter | `ctrl+s` `:w` `:q` `:wq` | Enregistrer, quitter, enregistrer et quitter |
| Fenêtres | `<C-h>` `<C-j>` `<C-k>` `<C-l>` | Aller à la fenêtre de gauche, en bas, en haut, à droite respectivement |
| Fenêtres | `<C-Up>` `<C-Down>` | Augmenter/diminuer la hauteur de la fenêtre |
| Fenêtres | `<C-Left>` `<C-Right>` | Diminuer/augmenter la largeur de la fenêtre |
| Buffers | `<S-h>` `<S-l>` | Buffer précédent/suivant |
| Buffers | `<leader>bb` `<leader>\`` | Basculer vers l'autre buffer |
| Recherche | `n` `N` | Résultat de recherche suivant/précédent |
| Recherche et remplacement | `:s/foo/bar/g` | Changer chaque 'foo' en 'bar' dans la ligne courante |
| Recherche et remplacement | `:%s/foo/bar/g` | Changer chaque 'foo' en 'bar' dans toutes les lignes |
| Enregistrement | `<C-s>` | Enregistrer le fichier |
| Divers | `<leader>K` | Keywordprg |
| Divers | `<leader>l` | Lazy |
| Divers | `<leader>fn` | Nouveau fichier |
| Divers | `<leader>xl` | Liste des emplacements |
| Divers | `<leader>xq` | Liste Quickfix |
| Divers | `<leader>cf` | Format |
| Divers | `<leader>cd` | Diagnostics de ligne |
| Divers | `<leader>uf` | Basculer le format automatique (global) |
| Divers | `<leader>uF` | Basculer le format automatique (buffer) |
| Divers | `<leader>us` | Basculer l'orthographe |
| Divers | `<leader>uw` | Basculer le retour à la ligne |
| Divers | `<leader>uL` | Basculer les numéros de ligne relatifs |
| Divers | `<leader>ul` | Basculer les numéros de ligne |
| Divers | `<leader>ud` | Basculer les diagnostics |
| Divers | `<leader>uc` | Basculer le masquage |
| Divers | `<leader>uh` | Basculer les indices en incrustation |
| Divers | `<leader>uT` | Basculer la mise en évidence de Treesitter |
| Divers | `<leader>ub` | Basculer l'arrière-plan |
| Divers | `<leader>gg` | Lazygit (répertoire racine) |
| Divers | `<leader>gG` | Lazygit (répertoire de travail actuel) |
| Divers | `<leader>qq` | Quitter tout |
| Divers | `<leader>ui` | Inspecter la position |
| Divers | `<leader>L` | Journal des modifications de LazyVim |
| Divers | `<leader>ft` | Terminal (répertoire racine) |
| Divers (Terminal) | `<leader>fT` | Terminal (répertoire de travail actuel) |
| Divers (Terminal) | `<c-/>` | Terminal (répertoire racine) |
| Divers | `<c-_>` | which_key_ignore |
| Divers | `<esc><esc>` | Entrer en mode normal |
| Divers | `<C-/>` | Masquer le terminal |
| Divers | `<leader>ww` | Autre fenêtre |
| Divers | `<leader>wd` | Supprimer la fenêtre |
| Divers | `<leader>w-` | Diviser la fenêtre ci-dessous |
| Divers | `<leader>-` | Diviser la fenêtre ci-dessous |
| Divers | `<leader>\|` | Diviser la fenêtre |
| Divers | `<leader><tab>l` | Dernier onglet |
| Divers | `<leader><tab>f` | Premier onglet |
| Divers | `<leader><tab><tab>` | Nouvel onglet |
| Divers | `<leader><tab>]` | Onglet suivant |
| Divers | `<leader><tab>d` | Fermer l'onglet |
| Divers | `<leader><tab>[` | Onglet précédent |
| LSP | `<leader>cl` | Infos Lsp |
| LSP | `gd` | Aller à la définition |
| LSP | `gr` | Références |
| LSP | `gD` | Aller à la déclaration |
| LSP | `gI` | Aller à l'implémentation |
| LSP | `gy` | Aller à la définition de type |
| LSP | `K` | Survole |
| LSP | `gK` | Aide à la signature |
| LSP | `<c-k>` | Aide à la signature |
| LSP | `<leader>ca` | Action de code |
| LSP | `<leader>cA` | Action de source |
| LSP | `<leader>cr` | Renommer |
| Indentation | `<<` /  `>>` | Indenter une ligne |
| Indentation | `<` /  `>` | Indenter la selection |
| Commandes Perso : | ---<br> | --- |
| Déplacement lignes | `<S-C-Down>` | Déplacer la ligne vers le bas |
| Déplacement lignes | `<S-C-Up>` | Déplacer la ligne vers le haut |
